# Angular Messages

`Angular Messages` is a demo application to show how to implement `JWT` authentication with a `Sails` backend and an `Angular` frontend.
[Read full article](http://angular-tips.com/blog/2014/05/json-web-tokens-examples/)

If you want to try it, you need to do the following:

Install the latest `sails` beta if you don't have it:

```
$ sudo npm install -g sails@beta
```

Then, inside this app folder:

```
$ npm install
$ sails lift
```

Register yourself and write some messages.

### TODO:

* Socket.io integration.
* Maybe different levels of authorization (i.e admin).
* Token expiration.

khaasfood.net old name server
NS1.CP-IN-10.WEBHOSTBOX.NET
NS2.CP-IN-10.WEBHOSTBOX.NET

ns1.servconfig.com
ns2.servconfig.com

NS1.AFRAID.ORG
NS2.AFRAID.ORG
NS3.AFRAID.ORG
NS4.AFRAID.ORG

NS1.SGP2.SITEGROUND.ASIA
NS2.SGP2.SITEGROUND.ASIA

● mongodb.service - High-performance, schema-free document-oriented database
   Loaded: loaded (/etc/systemd/system/mongodb.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2017-01-07 09:48:34 EST; 4 days ago
 Main PID: 16360 (mongod)
    Tasks: 14
   Memory: 52.7M
      CPU: 29min 11.552s
   CGroup: /system.slice/mongodb.service
           └─16360 /usr/bin/mongod --quiet --config /etc/mongod.conf

//////////////////////////////////////////
● mongodb.service - High-performance, schema-free document-oriented database
   Loaded: loaded (/etc/systemd/system/mongodb.service; enabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Thu 2017-01-12 03:14:35 EST; 37s ago
  Process: 19398 ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf (code=exited, status=2)
 Main PID: 19398 (code=exited, status=2)

Jan 12 03:14:35 everexpert.com systemd[1]: Started High-performance, schema-free document-oriented database.
Jan 12 03:14:35 everexpert.com mongod[19398]: Unrecognized option: systemLog.port
Jan 12 03:14:35 everexpert.com mongod[19398]: try '/usr/bin/mongod --help' for more information
Jan 12 03:14:35 everexpert.com systemd[1]: mongodb.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Jan 12 03:14:35 everexpert.com systemd[1]: mongodb.service: Unit entered failed state.
Jan 12 03:14:35 everexpert.com systemd[1]: mongodb.service: Failed with result 'exit-code'.