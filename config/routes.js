/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `config/404.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on routes, check out:
 * http://links.sailsjs.org/docs/config/routes
 */

module.exports.routes = {

  // Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, etc. depending on your
  // default view engine) your home page.
  //
  // (Alternatively, remove this and add an `index.html` file in your `assets` directory)
  '/': function (req,res) {

      // if(req.body.accessToken){
          // const BootBot = require('bootbot');
          // const bot = new BootBot({
          //     accessToken: $scope.accessToken,
          //     verifyToken : 'EAAajR22RhaIBAKCmVMrH864tDg0icZB6hjMHyX8hnvWEZA7DVLbLZALSgWM1qrDpU8KyYCD6NVSN28ZAsGnyuak5bqIwrKTw4L8WXwHp99fH7fAJU70WfmRnokgMX2wGtXNpvk8OghyyG59CO7OvX96EtjKM8BHlv3Puyx3FfnO19E9ZCSMmz',
          //     appSecret: '79f954cc4b1890703688ee9a8f52e829'
          // });
          //
          //
          // bot.on('message', (payload, chat) => {
          //     const text = payload.message.text;
          //     console.log(`The user said: ${text}`);
          // });
          //
          //     bot.hear(['hello', 'hi', /hey( there)?/i], (payload, chat) => {
          //         console.log('The user said "hello", "hi", "hey", or "hey there"');
          // });
          //
          // bot.start();
      // }

      return res.view ('homepage')
  },
  '/expertpanel':{
    view: 'admin',//  // view 'admin' in views directory will loaded automatically
    locals: {
      layout: 'admin/layout'
    }
  },
  '/conversion':{
    view: 'conversion',//  // view 'admin' in views directory will loaded automatically
    locals: {
      layout: 'static/layout'
    }
  }



  // Custom routes here...


  // If a request to a URL doesn't match any of the custom routes above,
  // it is matched against Sails route blueprints.  See `config/blueprints.js`
  // for configuration options and examples.

};
