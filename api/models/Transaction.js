/**
 * Transaction.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    tran_id: {
      type: 'string'
    },
    val_id: {
      type: 'string'
    },
    amount: {
      type: 'float'
    },
    card_type: {
      type: 'string'
    },
    store_amount: {
      type: 'float'
    },
    card_no: {
      type: 'string'
    },
    bank_tran_id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    tran_date: {
      type: 'date'
    },
    currency: {
      type: 'string'
    },
    card_issuer: {
      type: 'string'
    },
    card_brand: {
      type: 'string'
    },
    card_issuer_country: {
      type: 'string'
    },
    card_issuer_country_code: {
      type: 'string'
    },
    store_id: {
      type: 'string'
    },
    verify_sign: {
      type: 'string'
    },
    verify_key: {
      type: 'string'
    },
    currency_type: {
      type: 'string'
    },
    currency_amount: {
      type: 'float'
    },
    currency_rate: {
      type: 'float'
    },
    base_fair: {
      type: 'float'
    },
    value_a: {
      type: 'string'
    },
    value_b: {
      type: 'string'
    },
    value_c: {
      type: 'string'
    },
    value_d: {
      type: 'string'
    },
    risk_level: {
      type: 'string'
    },
    risk_title: {
      type: 'string'
    },
    error: {
      type: 'string'
    },
  }
};

