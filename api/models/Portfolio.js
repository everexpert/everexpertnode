/**
 * Blog.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    portfolioId:{
      type:'string'
    },
    portfolioTitle:{
      type:'string'
    },
    portfolioSlug:{
      type:'string'
    },
    portfolioDiscription:{
      type:'string'
    },
    portfolioImage:{
        type:'string'
    }
  }
};

