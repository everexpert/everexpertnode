var app = angular.module('adminApp', ['ui.router','ngFileUpload','toastr']);

app
    .run(function($rootScope, $state, Auth, $location, $window) {
        $rootScope
            .$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                if (!Auth.authorize(toState.data.access)) {
                    event.preventDefault();

                    $state.go('anon.login');
                }

                // if (Auth.isEditor()) {
                //
                //     if($location.path() == "/user"){
                //         event.preventDefault();
                //         // toastr.error('You are not allowed!', 'Error');
                //         $state.go('user.mediamanager');
                //     }
                // }

            });
    });
