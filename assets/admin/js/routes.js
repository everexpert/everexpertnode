angular.module('adminApp')
  .config(['$stateProvider', '$urlRouterProvider', 'AccessLevels', function($stateProvider, $urlRouterProvider, AccessLevels) {

    $stateProvider
        .state('anon', {
            abstract: true,
            template: '<ui-view/>',
            data: {
                access: AccessLevels.anon
            }
        })
        .state('anon.login', {
            url: '/login',
            templateUrl: 'admin/templates/auth/login.html',
            controller: 'LoginController'
        })
        .state('anon.register', {
          url: '/register',
          templateUrl: 'admin/templates/auth/register.html',
          controller: 'RegisterController'
        })
        .state('anon.recover', {
            url: '/recover',
            templateUrl: 'admin/templates/auth/recover.html',
            controller: 'RecoverController'
        })
        .state('anon.resetpassword', {
            url: '/resetpassword/:validationCode/:userEmail',
            templateUrl: 'admin/templates/auth/resetPassword.html',
            controller: 'ResetPasswordController'
        })
        .state('user', {
            abstract: true,
            templateUrl: 'admin/templates/base.html',
            data: {
                access: AccessLevels.user
            }
        })
       .state('user.home', {
            url: '/home',
            templateUrl: 'admin/templates/home/home.html',
            controller: 'HomeController'
        })
        .state('user.edit_settings', {
            url: '/edit-settings',
            templateUrl: 'admin/templates/settings/edit-settings.html',
            controller: 'EditSettingsController'
        })


        .state('user.portfolio', {
            url: '/portfolio',
            templateUrl: 'admin/templates/portfolio/portfolio.html',
            controller: 'PortfolioController'
        })
        .state('user.add_portfolio', {
            url: '/add-portfolio',
            templateUrl: 'admin/templates/portfolio/add-edit-portfolio.html',
            controller: 'AddPortfolioController'
        })
        .state('user.edit_portfolio', {
            url: '/edit-portfolio/:portfolioId',
            templateUrl: 'admin/templates/portfolio/add-edit-portfolio.html',
            controller: 'EditPortfolioController'
        })





        .state('user.mediamanager', {
            url: '/media-manager',
            templateUrl: 'admin/templates/media-manager/media.html',
            controller: 'MediaManagerController'
        })
        .state('user.user_record', {
            url: '/user',
            templateUrl: 'admin/templates/users/users.html',
            controller: 'UserController'
        });






    $urlRouterProvider.otherwise('/home');

  }]);
