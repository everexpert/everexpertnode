angular.module('adminApp')
  .controller('LoginController', function($scope, $state, Auth) {
    $scope.errors = [];

    $scope.login = function() {
      $scope.errors = [];
      Auth.login($scope.user)
          .then(function(result) {
                $state.go('user.home');
      },function(err) {
                console.log(err);
        $scope.errors.push(err.data.err);
      });
    }



  });
