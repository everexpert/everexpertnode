angular.module('adminApp')
    .controller('AddPortfolioController', ['$scope','$http','$stateParams','$state','Options',
        function ($scope, $http, $stateParams,$state,Options) {


        $scope.portfolio={};
        $scope.addOperation='Add';
        $scope.saveBlogs=function(){
            $http.post('/portfolio',$scope.portfolio).then(function(result){
                console.log(result);
                if(result){
                    $state.go("user.portfolio");
                }
            },function(err){
                if(err)
                    console.log(err);
            })
        };
        //convert title into slug
        $scope.makeTitleSlug = function(){
            $scope.portfolio.portfolioSlug = Options.convertToSlug( $scope.portfolio.portfolioTitle);
        };

        //Calling all image folders from body controller
        $scope.folderList();

        //image-select
        $scope.selectedImage=function (x) {
            $scope.portfolio.portfolioImage=x;
            console.log($scope.portfolio.portfolioImage);
            $('#myModal').modal('hide')
        }


    }]);

