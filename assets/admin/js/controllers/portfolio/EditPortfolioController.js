angular.module('adminApp')
  .controller('EditPortfolioController', ['$scope','$state','$http','$stateParams','Upload','Options','toastr',
      function ($scope,$state,$http,$stateParams,Upload,Options,toastr) {


        $scope.addOperation='edit';
        $scope.portfolio={};
        if($stateParams.hasOwnProperty("portfolioId")){
          $http({
            method: 'GET',
            url: '/portfolio/'+$stateParams.portfolioId
          }).then(function successCallback(response) {
            console.log(response);
            if(response.data){
              $scope.portfolio=response.data;
            }
          }, function errorCallback(response) {
            console.log(response)
          });
        }


        $scope.saveBlogs=function(){
          $http.post("/portfolio/update/"+$stateParams.portfolioId,$scope.portfolio).then(function(portfolio){
            if(portfolio){
              $state.go("user.portfolio");
            }
          },function(err){
            if(err)
              console.log(err);
          })
        };

        //convert title into slug
        $scope.makeTitleSlug = function(){
          $scope.blog.blogSlug = Options.convertToSlug( $scope.blog.blogTitle);
        };

        //Calling all image folders from body controller
        $scope.folderList();

        //image-select
        $scope.selectedImage=function (x) {
            $scope.portfolio.portfolioImage=x;
            console.log($scope.portfolio.portfolioImage);
            $('#myModal').modal('hide')
        }



  }]);
