angular.module('adminApp')
  .factory('CurrentUser', function(LocalService) {
      var currentUser;
    return {
      user: function() {
        if (LocalService.get('auth_teken')) {
          return currentUser=angular.fromJson(LocalService.get('auth_teken')).data.user;
        } else {
          return currentUser={};
        }
      }
    };
  });
