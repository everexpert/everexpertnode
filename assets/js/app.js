angular.module('app', ['ui.bootstrap', 'ui.router', 'ngMessages', 'app-templates','ngSanitize','ngMaterial'])
    .config(["$locationProvider", function($locationProvider) {

        $locationProvider.html5Mode(true);
    }]);





    $(document).ready(function() {
        $("#owl-demo4").owlCarousel({
            autoPlay : 3000,
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            //singleItem:true
            // "singleItem:true" is a shortcut for:
            items : 4,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            itemsTablet: true,
            itemsMobile : true
        });

    });
    $(document).ready(function() {
        $("#owl-demo5").owlCarousel({
            autoPlay : 3000,
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            //singleItem:true
            // "singleItem:true" is a shortcut for:
            items : 4,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            itemsTablet: true,
            itemsMobile : true
        });
    });
    window.addEventListener("orientationchange", function() {
        location.reload();
    });