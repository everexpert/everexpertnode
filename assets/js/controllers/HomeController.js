angular.module('app')
    .controller('HomeController', ['$scope','$http', function ($scope,$http) {


        $scope.portfolios=[];
        $http.get("portfolio").then(function(result){
            if(result.data.length){
                $scope.portfolios=result.data;
                console.log($scope.portfolios);
            }
        },function(err){
            if(err)
                console.log(err);
        });


        //HomeBanner
        // $('.ee_banner').slick({
        //     infinite: true,
        //     dots: false,
        //     slidesToShow: 1,
        //     slidesToScroll:1
        // });

        //TheyHaveSaidBanner
        $('.ee_clientSaid').slick({
            infinite: true,
            dots: true,
            slidesToShow: 1,
            autoplay: true
        });

        //PleasedToServeBanner
        $('.ee_serviceClients').slick({
            infinite: true,
            dots: false,
            slidesToShow: 4
        });

        $(".nano").nanoScroller();


        $scope.category=[
            {
                name:'application & <br/> software development',
                image:'images/ee_short_image/charity-app.png',
                ee_class:'ee_softwaredevelopmentim'
            },
            {
                name:'web <br/> development',
                image:'images/ee_short_image/html.png',
                ee_class:'ee_webdevelopmentIm'
            },
            {
                name:'data analytics & <br/> visualization',
                image:'images/ee_short_image/padlock.png',
                ee_class:'ee_desitalmarketingim'
            },
            {
                name:'exclusive <br/> products',
                image:'images/ee_short_image/web-design.png',
                ee_class:'ee_exclusiveproductim'
            }
        ];

        $scope.list=[
            {
                name:'application & <br/> software development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'application & <br/> software development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'application & <br/> software development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'web <br/> development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'web <br/> development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'web <br/> development',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },

            {
                name:'digital marketing & <br/> branding',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'digital marketing & <br/> branding',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            },
            {
                name:'digital marketing & <br/> branding',
                text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy',
            }
        ];

        $scope.feature=function (x) {
            console.log(x)
            for (var i=0; i<$scope.list.length;i++){
                if(x.name==$scope.list[i].name){
                    $scope.list[i].flag=true;
                }else {
                    $scope.list[i].flag=false;
                }
            }

            for(var ik=0; ik<$scope.category.length;ik++){
                if(x.name==$scope.category[ik].name){
                    $scope.category[ik].ee_active='active';
                }else {
                    $scope.category[ik].ee_active='inactive';
                }
            }

        };

        $scope.feature({name:$scope.category[0].name})

        angular.element(document).ready(function () {
            var $grid = $('.grid').imagesLoaded( function() {
                // init Isotope after all images have loaded
                $grid.isotope({
                    // options...
                });
            });
        })

    }]);
