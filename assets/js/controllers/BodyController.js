var app=angular.module('app');
app.controller('BodyController', ['$scope','$mdDialog','$window','$http', function ($scope, $mdDialog, $window, $http) {
        $(window).scroll(function (event) {
            if($(window).scrollTop()>200){
                $( "#mainNavSt" ).addClass('ee_sticky')
            }else{
                $( "#mainNavSt" ).removeClass('ee_sticky')
            }
        });

        $(".nano").nanoScroller();

        $scope.showAdvanced = function(ev,id) {
            $mdDialog.show({
                controller: 'ViewDetailsController',
                templateUrl: '../templates/viewDetails.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {
                    item: id
                },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function(answer) {
                // console.log(answer)
            }, function() {
                // console.log(answer)
            });

        };


        /*****=============Facebook==============*******/
        function statusChangeCallback(response,sCC) {
            if (response.status === 'connected') {
                console.log('connected');
            } else {
                console.log('NOtconnected');
            }
        }

        $window.checkLoginState=function(sCC) {
            FB.getLoginStatus(function(response) {
                var accessToken=response.authResponse.accessToken;
                if (response.status === 'connected') {
                    console.log('connected');
                    $scope.fbLogin(sCC,accessToken)
                } else {
                    console.log('NOtconnected');
                }
                statusChangeCallback(response,sCC);
            });
        };

        $window.fbAsyncInit = function() {
            // Executed when the SDK is loaded
            FB.init({
                appId: '1868377036522914',
                channelUrl: 'https://developers.facebook.com/docs/internationalization#plugins',
                status: true,
                cookie: true,
                xfbml: true,
                version    : 'v2.11'
            });

            FB.getLoginStatus(function(response) {
                console.log(response,'response');
                statusChangeCallback(response);
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $scope.fbLogin=function(sCC,accessToken) {
            $scope.accessToken=accessToken;
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me',function(fb_response) {
                console.log(fb_response)
            });

        };



        // $scope.FbMessage=function () {
        //    var fb_response= localStorage.getItem("fb_response");
        //    var accessToken= localStorage.getItem("accessToken");
        //     console.log(fb_response,accessToken)
        //
        //     FB.api('/me', { locale: 'en_US', fields: 'pages_messaging' },function(fbResponse) {
        //         $http.post('/facebookBot/facebookTest',{loginInfo:fb_response,accessToken:accessToken}).then(function(response){
        //             console.log(response)
        //         },function(err){
        //             if(err)
        //                 console.log(err);
        //         });
        //
        //     });
        // };




        $scope.Fblogout=function() {
            console.log("Fblogout")
            FB.logout(function(response) {
                console.log(response)
            });
        }










    }]);




app.directive('eeCompanyDirective',function($timeout){
    return {
        restrict: 'A',
        link: function(scope,element,attrs) {
            $timeout(function() {
                $(".ee_eeCompany").not('.slick-initialized').slick({
                    dots: false,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    arrows:false
                });
            });
        }
    }
});