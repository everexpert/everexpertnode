angular.module('app')
    .controller('ViewDetailsController', ['$scope','$mdDialog','item', function ($scope, $mdDialog, item ) {

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.jsonArray=[
            {
                id:'ee_1',
                name:'Web Application Development and Maintenance',
                details:'Monitoring and Evaluaiton System(MIS), Ecommerce Application, Service Center Automation, Online Table Reservation for Restaurant, Human Resource Management,\n' +
                'GIS based app.'
            },
            {
                id:'ee_2',
                name:'Desktop Application Development and Maintenance',
                details:'Supply Chain Management, Puzzle game development, Cross Platform Desktop App using Electron, Python Desktop Application.'
            },
            {
                id:'ee_3',
                name:'Mobile Application Development and Maintenance',
                details:'We develop native android mobile app and also hybrid app using Ionic2\n' +
                'framework, React native framework.'
            },
            {
                id:'ee_4',
                name:'GIS & GPS based App Development',
                details:'GEO Mapping, GPS tracking and GEO plotting with Google map or Open Street Map using different interactive JS Library.'
            },
            {
                id:'ee_5',
                name:'CMS customization and On demand Custom Service',
                details:'Payment gateway integration, SMS gateway integration,\n' +
                'Wordpress/Joomla Template/Plugins/Module Customization.'
            },
            {
                id:'ee_6',
                name:'Notification management system',
                details:'SMS Notification, Email Notification, Facebook Messenger Notification'
            },
            {
                id:'ee_7',
                name:'Consultancy, Documentation, Presentation',
                details:'Requirement Analysis, Prepare RFP/EOI Documentation, Software Architecture Define,\n' +
                'Technology Stack Choose, Software development team building, Project Management Ecosystem Development,\n' +
                'Server Optimization, Website Optimization.'
            },
            {
                id:'ee_8',
                name:'Data Analytics and Visualization',
                details:'We analyze vehicle tracking and ride sharing data and predict meaningful information.'
            },
            {
                id:'ee_9',
                name:'Web Scrapping',
                details:'We provide web scrapping with python and nodejs scripting.'
            },
            {
                id:'ee_10',
                name:'Web Service',
                details:'We are specialised to provide Rest API based web server with any\n' +
                'technology stack (LAMP, MEAN).'
            },
            {
                id:'ee_11',
                name:'DevOps & Cloud Server Management',
                details:'We provide DevOps Service and solution based on Amazon Web Services, Google Cloud\n' +
                'Platform and Digital Ocean cloud server ecosystem.'
            },
            {
                id:'ee_12',
                name:'Digital Marketing (Email, SMS, Facebook, Youtube)',
                details:'Social Media Marketing (Facebook, Twitter,\n' +
                'Youtube etc.), Email Campaign, SMS Campaign.'
            },
            {
                id:'ee_13',
                name:'Search Engine Optimization',
                details:'Keyword Researcher, On Page SEO(Onsite SEO, Technical SEO), Off Page SEO.'
            },
            {
                id:'ee_14',
                name:'Corporate Branding',
                details:'Logo Sketch, Design, Mockup, Letterhead, Business card, Envelop, Branding'
            },
            {
                id:'ee_15',
                name:'UX/UI Design',
                details:'Requirement Analysis, User Experience Design, User Interface Design,\n' +
                'Web Application Design, Mobile Application Design, Prototyping, Mockup Design.'
            },
            {
                id:'ee_16',
                name:'Photography & Video',
                details:'Product Photography, Industry Photography, Building Photography, HD Video Capturing and Editing.'
            },
            {
                id:'ee_17',
                name:'GIS based application',
                details:'GEO Plotting pollings station all over bangladesh with leaflet js.'
            },
            {
                id:'ee_18',
                name:'MIS, Monitoring and Evaluation System',
                details:'NGO based organization.'
            },
            {
                id:'ee_19',
                name:'Hotel ERP',
                details:'Room Reservation, Folio Management, Restaurant Table Reservation, Accounting, Kitchen Manadement, Laundry Service Management, Transportation Management, Point of Sale, Purchase, Inventory, HR & Payroll System, Leave Management, Web portal management, Online Reservation System,Integration with GDS.'
            },
            {
                id:'ee_20',
                name:'All Purpose ERP',
                details:'Full Accounting Module, Financial Module, Procurement Module, Sales Module, Manufacturing Module, Human Resource Module, Payroll Module, Inventory Module, etc.'
            },
            {
                id:'ee_21',
                name:'Ecommerce based Applicaiton',
                details:'Product Gallery, Nested Product Category, Product Tagging, Product Review System, Easy guided Shopping cart,\n' +
                'payment gateway integration, SMS gateway integration, facebook messanger integraton,\n' +
                'real time chat integration, order report panel, customer dashboard panel, enhanced admin panel,\n' +
                'role based permission in admin panel.'
            },
            {
                id:'ee_22',
                name:'Social Networking',
                details:'Sentiment Analysis, Using Facebook Graph API and graph database(neo4j) we have develop a network based facebook group and page.\n' +
                'In the project, we have analysis user interaction on those pages and groups. As a result we have go the most active user with\n' +
                'certain mentality.Also we have develop matrimonial website with Real time chat feature.'
            },
            {
                id:'ee_23',
                name:'Vehicle Tracking and Ride Sharing',
                details:'Using XMPP and Socket programming, we have develop Uber/Pathao like ride sharing app with realtime tracking dispatcher dashboard for monitoring\n' +
                'overll process.'
            },
            {
                id:'ee_24',
                name:'Online Reservation',
                details:'Online/Offline Hotel/Restaurant reservation.'
            },
            {
                id:'ee_25',
                name:'Human Resource Management System',
                details:'Online/Offline Hotel/Restaurant table reservation.'
            },
            {
                id:'ee_26',
                name:'Supply Chain Management System',
                details:'Online/Offline Hotel/Restaurant table reservation.'
            }

            ];


        $scope.viewDialog=$scope.jsonArray.find(function (items) {
            if (items.id===item.id){
                return items
            }
        });


    }]);
