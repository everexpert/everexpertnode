angular.module('app')
  .factory('Auth', function($http, LocalService, AccessLevels,CurrentUser,$q) {
    return {
      authorize: function(access) {
        if (access === AccessLevels.user) {
          return this.isAuthenticated();
        } else {
          return true;
        }
      },
      isAuthenticated: function() {
        return LocalService.get('auth_token');
      },

      login: function(credentials) {
        var deferred = $q.defer();
        console.log('credentials',credentials);
        var login = $http.post('/auth/authenticate', credentials);

        return $q(function(resolve, reject) {
          login.then(function(result) {
            LocalService.set('auth_token', JSON.stringify(result));
              resolve(result);
              // console.log(result);
          },function(err){
            if(err)
              reject(err);
              console.log(err);
          });
        });



      },

      logout: function() {
        // The backend doesn't care about logouts, delete the token and you're good to go.
        LocalService.unset('auth_token');
      },

      register: function(formData) {
        LocalService.unset('auth_token');
        var register = $http.post('/auth/register', formData);
        return $q(function(resolve, reject) {
          register.then(function(result) {
            LocalService.set('auth_token', JSON.stringify(result));
            resolve(result);
          },function(err){
            if(err)
              reject(err);
            console.log(err);
          });
        });


      },

      recover: function(user) {
        return $http.post('/auth/recover',user);
      },
      // oldUser: function(user) {
      //   console.log(user);
      //   return $http.post('/auth/olduser',user);
      // },

      checkVarification: function(params) {
        var codeVerification = $http.post("auth/checkVarification",params);
        return $q(function(resolve, reject) {
          codeVerification.then(function(response) {
            console.log(response);
            resolve(response);
          },function(err){
            if(err)
              reject(err);
            console.log(err);
          });
        });
      },

      resetPassword:function(params){
        var resetPss = $http.post('auth/resetPassword',params);
        return $q(function(resolve, reject) {
          resetPss.then(function(response) {
            console.log(response);
            resolve(response);
          },function(err){
            if(err)
              reject(err);
            console.log(err);
          });
        });
      }

    }
  })


  .factory('AuthInterceptor', function($q, $injector) {
    var LocalService = $injector.get('LocalService');

    return {
      request: function(config) {
        var token;
        if (LocalService.get('auth_token')) {
          token = angular.fromJson(LocalService.get('auth_token')).token;
        }
        if (token) {
          config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
      },
      responseError: function(response) {
        if (response.status === 401 || response.status === 403) {
          LocalService.unset('auth_token');
          //$injector.get('$state').go('anon.login');
        }
        return $q.reject(response);
      }
    }
  })
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });
